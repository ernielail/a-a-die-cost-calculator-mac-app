//
//  ViewController.swift
//  A&A Die Cost Calculator
//
//  Created by Ernie Lail on 3/26/18.
//  Copyright © 2018 Maranatha Technologies. All rights reserved.
//

import Cocoa

class ViewController: NSViewController{
    
    
    
    
    @IBOutlet weak var numOfImpos: NSTextField!
    
    @IBOutlet weak var extLabel: NSTextField!
    @IBOutlet weak var extCutWidth: NSTextField!
    
    @IBOutlet weak var extCutHeight: NSTextField!
    
    @IBOutlet weak var numOfHangLabels: NSTextField!
    @IBOutlet weak var numOfHangers: NSTextField!
    
    @IBOutlet weak var intCutLabel: NSTextField!
    @IBOutlet weak var intCutWidth: NSTextField!
    
    @IBOutlet weak var xLabel: NSTextField!
    @IBOutlet weak var intCutHeight: NSTextField!
    
    
    @IBOutlet weak var numOfImposLabel: NSTextField!
    
    @IBOutlet weak var costField: NSTextField!
    
    @IBOutlet weak var detailField: NSTextField!
    
    @IBOutlet weak var calcBtn: NSButton!
    @IBOutlet weak var calcBtn2: NSButton!
    
    
    @IBOutlet weak var checkBox: NSButton!
    
    
    
    @IBOutlet weak var lengthOfRules: NSTextField!
    
    @IBOutlet weak var lengthLabel: NSTextField!
    
    
    @IBOutlet weak var xBtn3: NSTextField!
    @IBOutlet weak var xBtn2: NSTextField!
    
    @IBOutlet weak var xBtn: NSTextField!
    
    
    @IBOutlet weak var boardSize: NSTextField!
    @IBOutlet weak var boardHeight: NSTextField!
    @IBOutlet weak var boardWidth: NSTextField!
    
    
    
    @IBOutlet weak var intCutGrouping: NSBox!
    @IBOutlet weak var table: NSScrollView!

    @IBOutlet weak var tableView: NSTableView!
    
    @IBOutlet weak var addBtn: NSButton!
    
    @IBOutlet weak var resetBtn: NSButton!
    var intCuts = [[String:String]]()
    
    @IBAction func addPressed(_ sender: Any) {
   
        let arrayItem = ["width":intCutWidth.stringValue, "height":intCutHeight.stringValue]
        intCuts.append(arrayItem)
        self.tableView.reloadData()

    }
    
    @IBAction func resetPress(_ sender: Any) {
  intCuts.removeAll()
        
        self.tableView.reloadData()

    }
    
    
    func hideDetailed(){
        extLabel.isHidden = true
        extCutWidth.isHidden = true
        extCutHeight.isHidden = true
        intCutLabel.isHidden = true
        intCutWidth.isHidden = true
        xLabel.isHidden = true
        intCutHeight.isHidden = true
        numOfImpos.isHidden = true
        numOfImposLabel.isHidden = true
        calcBtn.isHidden = true
        xBtn.isHidden = true
        xBtn3.isHidden = true
intCutGrouping.isHidden = true
 table.isHidden = true
        addBtn.isHidden = true
        resetBtn.isHidden = true

        
        boardSize.isHidden = false
        boardHeight.isHidden = false
        boardWidth.isHidden = false
        lengthLabel.isHidden = false
        lengthOfRules.isHidden = false
        calcBtn2.isHidden = false
        xBtn2.isHidden = false
        
        numOfHangLabels.stringValue = "Number of Punches in Die"
        numOfHangers.stringValue = ""
       

    }
    
    func hideManual(){
        extLabel.isHidden = false
        extCutWidth.isHidden = false
        extCutHeight.isHidden = false
        intCutLabel.isHidden = false
        intCutWidth.isHidden = false
        xLabel.isHidden = false
        intCutHeight.isHidden = false
        numOfImpos.isHidden = false
        numOfImposLabel.isHidden = false
        calcBtn.isHidden = false;
        xBtn.isHidden = false
        xBtn3.isHidden = false
        intCutGrouping.isHidden = false
        table.isHidden = false
        addBtn.isHidden = false
        resetBtn.isHidden = false

        boardSize.isHidden = true
        boardHeight.isHidden = true
        boardWidth.isHidden = true
        lengthLabel.isHidden = true
        lengthOfRules.isHidden = true
        calcBtn2.isHidden = true;
        xBtn2.isHidden = true
        
        numOfHangLabels.stringValue = "Number of Punches in 1 Up"
        numOfHangers.stringValue = ""
        
    }
   
    
    
    @IBAction func checkClick(_ sender: Any) {
    //if checked hide detailed calc boxes and show manual
        if checkBox.state == .on {
         hideDetailed()
        }
        else{
        //else show detailed calc and hide manual
    hideManual()
        }
    }
    
    
    
    
    
    
    @IBAction func manCalcPress(_ sender: Any) {
   
    //use total to determine correct price
    
        var dieBoardCost = 0.0
        var numHangers = 0.0

        let totalRule = Double(lengthOfRules.stringValue)
        
        if numOfHangers.stringValue != ""
        {
            numHangers = Double(numOfHangers.stringValue)!
        }
        
        
        if (totalRule)! < 300.0 {
            //die board cost based on square inches of impo X number of impos + 5%
            //then multiply by cost
            
            dieBoardCost =  Double(Double(boardHeight.stringValue)! * Double(boardWidth.stringValue)!) * Double(0.05)
            
            print("Total Rule Below 300 in, so we will be charged $"+String(dieBoardCost)+" for the die board.")
        }
        
        let ruleCost = Double(totalRule!) * Double(0.75)
        
        let hangerCost = (numHangers * 8.0)
        
        let finalCost = ((ruleCost + hangerCost)) + dieBoardCost
        
        
        
        
        print("numHangers: "+String(numHangers))
        
        
        print("totalRule: "+String(totalRule!))
        
        print("hangerCost: $"+String(hangerCost))
        print("ruleCost: $"+String(ruleCost))
        print("finalCost: $"+String(finalCost))
        print("DieBoard: $"+String(dieBoardCost))
     
        detailField.stringValue = "\nHangers: "
        detailField.stringValue += String(numHangers)+"\nRule Cost: $"+String(ruleCost)
        detailField.stringValue += "\n Hanger Cost: $"+String(hangerCost)
        
        detailField.stringValue += "\n---------------------"
        
        detailField.stringValue += "\nTotal Board Cost: $"+String(dieBoardCost)
        
        detailField.stringValue += "\nTotal Cost: $"+String(finalCost)
        
        
        let markedUp = Double(finalCost) * Double(1.20) + 50.0
        
        detailField.stringValue += "\nTotal Charged: $"+String(markedUp)
        
        costField.stringValue = "$"+String(markedUp.rounded(toPlaces: 2))

    }
    
    @IBAction func calcBtnPress(_ sender: Any) {
        
        //calc rule used
        
        
        var extTotal = 0.0
        var numHangers = 0.0
        var numImpos = 0.0

        if extCutWidth.stringValue != "" && extCutHeight.stringValue != ""
        {
            extTotal = (Double(extCutWidth.stringValue)! * 2.0 ) + (Double(extCutHeight.stringValue)! * 2.0)
        }
        
        var internals = 0.0

        for cuts in intCuts {
          print(cuts)
            
            let intW = Double(cuts["width"]!)
            let intH = Double(cuts["height"]!)
            let intTotal = (intW! * 2 ) + (intH! * 2)
            internals += intTotal
            
        }
        
       if numOfHangers.stringValue != ""
       {
        numHangers = Double(numOfHangers.stringValue)!
        }
        
        if numOfImpos.stringValue != ""
        {
            numImpos = Double(numOfImpos.stringValue)!
        }
        
       
        
        
        
        
        
        
        
        
        
        
        var dieBoardCost = 0.0

        let totalRule = (extTotal + Double(internals))
        
        
        
        
        if (totalRule * numImpos) < 300.0 {
 //die board cost based on square inches of impo X number of impos + 5%
            //then multiply by cost
            
            dieBoardCost = (((Double(extCutWidth.stringValue)! * Double(extCutHeight.stringValue)!) *  Double(numImpos)) * Double(1.05)) *  Double(0.05)
            
            print("Total Rule Below 300 in, so we will be charged $"+String(dieBoardCost)+" for the die board.")
        }
        
        let ruleCost = Double(totalRule) * Double(0.75)
        
        let hangerCost = (numHangers * 8.0)
        
        let finalCost = (numImpos * (ruleCost + hangerCost)) + dieBoardCost
        
        
       
        print("extTotal: "+String(extTotal))

        print("numHangers: "+String(numHangers))
        

        print("totalRule: "+String(totalRule))

        print("hangerCost: $"+String(hangerCost))
        print("ruleCost: $"+String(ruleCost))
        print("finalCost: $"+String(finalCost))
   print("DieBoard: $"+String(dieBoardCost))
        
        
        detailField.stringValue = "1 Up Specs: \nExterior Rules: "+String(extTotal)+"in"+"\nInterior Rules: "
        detailField.stringValue += String(internals)+"in \nHangers: "
        detailField.stringValue += String(numHangers)+"\nRule Cost: $"+String(ruleCost)
         detailField.stringValue += "\n Hanger Cost: $"+String(hangerCost)
        
   detailField.stringValue += "\n---------------------"
        
        detailField.stringValue += "\nTotal Rules: "+String(numImpos * (totalRule))
        
        detailField.stringValue += "\nTotal Rules Cost: $"+String(numImpos * (ruleCost))

        
        detailField.stringValue += "\nTotal Board Cost: $"+String(dieBoardCost)
        
            detailField.stringValue += "\nTotal Cost: $"+String(finalCost)
        
        
        let markedUp = (Double(finalCost) * Double(1.20)) + 50.0
        
         detailField.stringValue += "\nTotal Charged: $"+String(markedUp)
        
        costField.stringValue = "$"+String(markedUp.rounded(toPlaces: 2))

        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         hideManual()
       self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    
    
    
    
    
    
  
    
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension ViewController:NSTableViewDataSource, NSTableViewDelegate{
    func numberOfRows(in tableView: NSTableView) -> Int {
        return intCuts.count
    }
    
    fileprivate enum CellIdentifiers {
        static let Width = "Width"
        static let Height = "Height"
    }

    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?{
        var result:NSTableCellView
        result  = tableView.makeView(withIdentifier: (tableColumn?.identifier)!, owner: self) as! NSTableCellView
    if tableColumn == tableView.tableColumns[0] {
        result.textField?.stringValue = intCuts[row]["width"]!
        }
        else{
        result.textField?.stringValue = intCuts[row]["height"]!
        }
        return result
    }
    
}
