//
//  AppDelegate.swift
//  A&A Die Cost Calculator
//
//  Created by Ernie Lail on 3/26/18.
//  Copyright © 2018 Maranatha Technologies. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

